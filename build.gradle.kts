@file:Suppress("PropertyName", "UnstableApiUsage")
plugins {
    id("org.quiltmc.loom").version("0.12.+")
    `maven-publish`
    java
}

val group: String by project
val version: String by project
val archivesBaseName: String by project

val minecraft_version = libs.versions.minecraft.get()
val mappings = libs.versions.mappings.get()

sourceSets {
    create("testmod") {
        compileClasspath += sourceSets.main.get().compileClasspath
        runtimeClasspath += sourceSets.main.get().runtimeClasspath
    }
}

repositories {
    mavenLocal()
    mavenCentral()
    maven( "https://maven.gegy.dev" )
    maven( "https://repsy.io/mvn/enderzombi102/mc" )
    maven( "https://maven.terraformersmc.com/releases" )
    maven( "https://maven.quiltmc.org/repository/release" )
    maven( "https://maven.quiltmc.org/repository/snapshot" )
}

val testmodImplementation by configurations
dependencies {
    minecraft("com.mojang:minecraft:$minecraft_version")
    mappings( loom.layered {
        addLayer( quiltMappings.mappings( "org.quiltmc:quilt-mappings:$minecraft_version+build.$mappings:v2" ) )
    } )
    include(libs.bundles.include)
    implementation(libs.bundles.implementation)
    modImplementation(libs.bundles.mod.implementation)

    testmodImplementation( sourceSets.main.get().output )
}

loom {
    shareRemapCaches.set( true )

    runConfigs {
        create("testmodClient") {
            client()
            runDir = "run"
            setSource( sourceSets.getByName("testmod") )
        }
    }
}

tasks.withType<ProcessResources>().configureEach {
    inputs.property( "group"            , project.group )
    inputs.property( "version"          , version )
    inputs.property( "loader_version"   , libs.versions.loader.get() )
    inputs.property( "minecraft_version", minecraft_version )
    filteringCharset = "UTF-8"

    filesMatching( "quilt.mod.json" ) {
        expand(
            Pair( "group"            , project.group ),
            Pair( "version"          , version ),
            Pair( "loader_version"   , libs.versions.loader.get() ),
            Pair( "minecraft_version", minecraft_version )
        )
    }
}

tasks.withType<JavaCompile>().configureEach {
    options.encoding = "UTF-8"
    options.release.set( 17 )
}

java {
    toolchain {
        languageVersion.set( JavaLanguageVersion.of( 17 ) )
    }
    withSourcesJar()
}

tasks.withType<Jar>() {
    from( "LICENSE" ) {
        rename { "${it}_$archivesBaseName" }
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            // add all the jars that should be included when publishing to maven
            artifact( tasks.remapJar ) {
                builtBy( tasks.remapJar )
            }
            artifact( tasks["sourcesJar"] ) {
                builtBy( tasks.remapSourcesJar )
            }
        }
    }

    // select the repositories you want to publish to
    repositories {
        mavenLocal()
        maven {
            name = "Repsy"
            credentials( PasswordCredentials::class )
            url = uri( "https://repsy.io/mvn/enderzombi102/mc" )
        }
    }
}