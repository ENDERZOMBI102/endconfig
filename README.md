EndConfig
-
Yet another minecraft config library, but this time, compatible with both quilt and fabric, across many versions!

Features:
-
- Automatic config syncing between client and server
- Config screen generation
- Automatic save/loading from disk on change
