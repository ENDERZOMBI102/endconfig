package com.enderzombi102.test;

import com.enderzombi102.endconfig.api.EndConfig;
import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;

public class ModMenuEntrypoint implements ModMenuApi {
	static {
		EndConfig.register( "testmod", ConfigData.class );
	}

	@Override
	public ConfigScreenFactory<?> getModConfigScreenFactory() {
		return EndConfig.getHolder( "testmod" )::screen;
	}
}
